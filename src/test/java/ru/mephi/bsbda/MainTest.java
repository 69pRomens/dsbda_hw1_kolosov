package ru.mephi.bsbda;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import ru.mephi.bsbda.mappers.MainMapper;
import ru.mephi.bsbda.reducers.MainReducer;
import ru.mephi.bsbda.types.Traffic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainTest {
    MapDriver<Object, Text, Text, Traffic> mapperDriver;
    ReduceDriver<Text, Traffic, Text, Text> reduceDriver;

    @Before
    public void setUp() {
        MainMapper mapper = new MainMapper();
        MainReducer reducer = new MainReducer();
        mapperDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testMainMapper() throws IOException {
        mapperDriver.withInput(new LongWritable(), new Text("46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] \"GET /administrator/ HTTP/1.1\" 200 4263 \"-\" \"Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0\" \"-\""));
        mapperDriver.withInput(new LongWritable(), new Text("109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] \"POST /administrator/index.php HTTP/1.1\" 200 4494 \"http://almhuette-raith.at/administrator/\" \"Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0\" \"-\""));
        mapperDriver.withOutput(new Text("46.72.177.4"), new Traffic(new Text("46.72.177.4"), new IntWritable(4263)));
        mapperDriver.withOutput(new Text("109.169.248.247"), new Traffic(new Text("109.169.248.247"), new IntWritable(4494)));

        mapperDriver.runTest();
    }


    @Test
    public void testMainReducer() throws IOException {
        List<Traffic> values1 = new ArrayList<Traffic>();
        values1.add(new Traffic(new Text(), new IntWritable(1220)));
        values1.add(new Traffic(new Text(), new IntWritable(2346)));
        values1.add(new Traffic(new Text(), new IntWritable(1234)));
        values1.add(new Traffic(new Text(), new IntWritable(5555)));
        reduceDriver.withInput(new Text("109.169.248.247"), values1);

        List<Traffic> values2 = new ArrayList<Traffic>();
        values2.add(new Traffic(new Text(), new IntWritable(123)));
        values2.add(new Traffic(new Text(), new IntWritable(234)));
        values2.add(new Traffic(new Text(), new IntWritable(124)));
        values2.add(new Traffic(new Text(), new IntWritable(555)));
        reduceDriver.withInput(new Text("95.140.24.131"), values2);

        reduceDriver.withOutput(new Text("109.169.248.247"), new Text("2588.75;10355;"));
        reduceDriver.withOutput(new Text("95.140.24.131"), new Text("259.0;1036;"));

        reduceDriver.runTest();
    }

}