package ru.mephi.bsbda;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import ru.mephi.bsbda.mappers.MainMapper;
import ru.mephi.bsbda.types.Traffic;
import ru.mephi.bsbda.reducers.MainReducer;


/**
 * @author R. Kolosov
 */
public class Main extends Configured implements Tool {

    /**
     * @param args first and second args takes input files, third param take output file
     * @throws Exception Program Errors
     */
    public static void main(final String args[]) throws Exception {
        Configuration conf = new Configuration();
        int res = ToolRunner.run(conf, new Main(), args);
        System.exit(res);
    }

    /**
     * Run method which schedules the Hadoop Job
     * @param args Arguments passed in main function
     */
    public int run(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator", ";");

        Job job = Job.getInstance(conf, "MapReduce JOB");

        job.setJarByClass(Main.class);
        job.setReducerClass(MainReducer.class);

        //Map
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Traffic.class);

        //Reduce
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(4);

        //Input files
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MainMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MainMapper.class);

        //Output file
        Path outputPath = new Path(args[2]);
        FileOutputFormat.setOutputPath(job, outputPath);
        job.setOutputFormatClass(TextOutputFormat.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
