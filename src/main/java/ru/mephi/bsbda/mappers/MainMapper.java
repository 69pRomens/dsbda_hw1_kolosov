package ru.mephi.bsbda.mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.mephi.bsbda.types.Traffic;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Map Class which extends mapreduce.Mapper class
 * It handle http server log file
 * @author Kolosov
 */
public class MainMapper extends Mapper<Object, Text, Text, Traffic> {

    private final static String pattern = "^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"([^\"]+)\" \"([^\"]+)\"";

    private Text ip = new Text();
    private IntWritable bytes = new IntWritable();

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String record = value.toString();

        Pattern r = Pattern.compile(pattern);

        Matcher matcher = r.matcher(record);

        if (!matcher.matches()) return;

        ip.set(matcher.group(1));
        bytes.set(Integer.parseInt(matcher.group(7)));

        Traffic traffic = new Traffic(ip, bytes);

        context.write(ip, traffic);
    }

}
