package ru.mephi.bsbda.reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import ru.mephi.bsbda.types.Traffic;

import java.io.IOException;

/**
 * Reduce class which is executed after the map class and takes
 * key and corresponding values, calculate average all the values and total sum of bytes
 *
 * @author Kolosov
 */
public class MainReducer extends Reducer<Text, Traffic, Text, Text> {

    private Text result = new Text();

    /**
     * Method which performs the join operation, calculate average and total bytes
     */
    public void reduce(Text key, Iterable<Traffic> values, Context context) throws IOException, InterruptedException {
        double average = 0;
        int total = 0;
        double i = 0;
        String tmp = "";

        for (Traffic traffic : values) {
            total += Integer.parseInt(traffic.getBytes().toString());
            i++;
        }

        average = total / i;

        tmp = average + ";" + total + ";";

        result.set(tmp);

        context.write(key, result);
    }
}
