package ru.mephi.bsbda.types;

import com.google.common.base.Objects;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Traffic implements WritableComparable<Traffic> {

    private Text ip;
    private IntWritable bytes;

    public Traffic() {
        ip = new Text();
        bytes = new IntWritable();
    }

    public Traffic(Text ip, IntWritable bytes) {
        this.ip = ip;
        this.bytes = bytes;
    }

    public void set(Text ip, IntWritable bytes)
    {
        this.ip = ip;
        this.bytes = bytes;
    }

    public Text getIp() {
        return ip;
    }

    public void setIp(Text ip) {
        this.ip = ip;
    }

    public IntWritable getBytes() {
        return bytes;
    }

    public void setBytes(IntWritable bytes) {
        this.bytes = bytes;
    }

    public int compareTo(Traffic o) {
        Text thisV = this.ip;
        Text anotherV = o.ip;
        return thisV.compareTo(anotherV);
    }

    public void write(DataOutput dataOutput) throws IOException {
        ip.write(dataOutput);
        bytes.write(dataOutput);
    }

    public void readFields(DataInput dataInput) throws IOException {
        ip.readFields(dataInput);
        bytes.readFields(dataInput);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Traffic traffic = (Traffic) o;
        return Objects.equal(ip, traffic.ip) &&
                Objects.equal(bytes, traffic.bytes);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(ip, bytes);
    }

    @Override
    public String toString() {
        return "Traffic{" +
                "ip=" + ip +
                ", bytes=" + bytes +
                '}';
    }
}